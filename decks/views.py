from django.shortcuts import render, get_object_or_404
from decks.models import (
    Deck,
    AllyCard, ChaosCard, CombatClassCard,
    PowerCard, UpgradeCard, SymbolCard,
    MissionCard, LeaderCard,
    SummonedSidekickCard,
)


# Deck Views
def show_deck(request, id):  # Modify classes or views to adjust based on card class type
    deck_object = get_object_or_404(Deck, id=id)
    d = deck_object.deckName
    cards = []
    if d == "Cape":
        for card in AllyCard.objects.all():
            cards.append(card)
        for p_card in PowerCard.objects.all():
            cards.append(p_card)
        for u_card in UpgradeCard.objects.all():
            cards.append(u_card)
    elif d == "allycards":
        cards = AllyCard.objects.all()
    elif d == "Chaos":
        cards = ChaosCard.objects.all()
    elif d == "Symbol":
        cards = SymbolCard.objects.all()
    elif d == "Combat Class":
        cards = CombatClassCard.objects.all()
    elif d == "Mission":
        cards = MissionCard.objects.all()
    elif d == "Leader":
        cards = LeaderCard.objects.all()
    elif d == "Summoned Sidekick":
        cards = SummonedSidekickCard.objects.all()
    context = {
        "deck_object": deck_object,
        "cards": cards,
    }
    return render(request, "decks/detail.html", context)


def list_decks(request):
    decks = Deck.objects.all()
    context = {
        "decks": decks,
    }
    return render(request, "decks/decks.html", context)


# Card Views
def show_card(request, cardType, id):
    card = get_object_or_404(cardType, id=id)
    context = {
        "card_object": card
    }
    return render(request, "cards/detail.html", context)


def ally_cards(request):
    cards = AllyCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/allycards.html", context)


def chaos_cards(request):
    cards = ChaosCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)


def power_cards(request):
    cards = PowerCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)


def upgrade_cards(request):
    cards = UpgradeCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)


def symbol_cards(request):
    cards = SymbolCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)


def combat_class_cards(request):
    cards = CombatClassCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)


def mission_cards(request):
    cards = MissionCard.objects.all()
    context = {
        "cards": cards,
    }
    return render(request, "cards/detail.html", context)
