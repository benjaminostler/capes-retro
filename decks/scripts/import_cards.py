import csv
from decks.models import AllyCard, ChaosCard, Deck, LeaderCard, MissionCard, SummonedSidekickCard, PowerCard, UpgradeCard, SymbolCard, CombatClassCard

def save_card_name_classname_deck_and_image(card, row):
    card.cardName = row['cardName']
    card.cardImg = row['cardImg']
    card.className = row['className']
    card.deck = Deck.objects.get(deckName=row['deck'])
    card.save()
    print(f"Saved {card.cardName} from {card.deck.deckName}")

def import_cards(csv_file_path):
    with open(csv_file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            deck_name = row['deck']
            className = row['className']
            if deck_name == 'Brawler Cape' or deck_name == 'Blaster Cape':
                if className == 'Hero' or className == 'Superhero' or className == 'Sidekick':
                    card = AllyCard()
                    card.comboNum1 = row['comboNum1']
                    card.comboNum2 = row['comboNum2']
                    card.comboNum3 = row['comboNum3']
                    save_card_name_classname_deck_and_image(card, row)
                elif className == 'Power':
                    card = PowerCard()
                    save_card_name_classname_deck_and_image(card, row)
                elif className == 'Upgrade':
                    card = UpgradeCard()
                    save_card_name_classname_deck_and_image(card, row)
            elif deck_name == 'Chaos':
                card = ChaosCard()
                save_card_name_classname_deck_and_image(card, row)
            else:
                if deck_name == 'Symbol':
                    card = SymbolCard()
                    save_card_name_classname_deck_and_image(card, row)
                elif deck_name == 'Leader':
                    card = LeaderCard()
                    save_card_name_classname_deck_and_image(card, row)
                elif deck_name == 'Mission':
                    card = MissionCard()
                    save_card_name_classname_deck_and_image(card, row)
                elif deck_name == 'Combat Class':
                    card = CombatClassCard()
                    save_card_name_classname_deck_and_image(card, row)
                elif deck_name == 'Summoned Sidekick':
                    card = SummonedSidekickCard()
                    save_card_name_classname_deck_and_image(card, row)

            # # if the deck doesn't exist, create it
            # card.deck, created = Deck.objects.get_or_create(deckName=row['deck'])  # set the deck

def run():
    import_cards('./ally_cards.csv')
    # import_cards('./power_cards.csv')
    # import_cards('./upgrade_cards.csv')
    # import_cards('./chaos_cards.csv')
    # import_cards('./symbol_cards.csv')
    # import_cards('./leader_cards.csv')
    # import_cards('./mission_cards.csv')
    # import_cards('./combat_class_cards.csv')
    # import_cards('./summoned_sidekick_cards.csv')
    