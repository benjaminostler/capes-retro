import csv
from decks.models import Deck 

def import_decks(csv_file_path):
    with open(csv_file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            deck_name = row['deckName']
            print("deck_name: ", deck_name)
            description = row['description']
            print("description: ", description)
            deck = Deck()
            deck.deckName = deck_name
            deck.description = description
            # if the deck has an image,
            if row['deckImg']:
                # save the image
                deck.deckImg = row['deckImg']
                print("deckImg: ", deck.deckImg)
            # if deck already exists in database, update it
            deck, created = Deck.objects.get_or_create(deckName=deck_name, description=description, deckImg=deck.deckImg)
            deck.save()
            print(f"Saved {deck.deckName} deck")

def run():
    csv_file_path = './decks.csv'
    import_decks(csv_file_path)
