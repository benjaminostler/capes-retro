from django.urls import path
from decks.views import (
    show_deck, list_decks, show_card,
    ally_cards, chaos_cards,
    combat_class_cards, mission_cards,
    power_cards, upgrade_cards, symbol_cards
)

urlpatterns = [
    # path('<str:deckName>/', show_deck, name="show_deck"),
    path('', list_decks, name="list_decks"),
    path('<int:id>/', show_card, name="show_card"),
    path('allycards/', ally_cards, name="ally_cards"),
    path('chaoscards/', chaos_cards, name="chaos_cards"),
    path('combatclasscards/', combat_class_cards, name="combat_class_cards"),
    path('powercards/', power_cards, name="power_cards"),
    path('upgradecards/', upgrade_cards, name="upgrade_cards"),
    path('missioncards/', mission_cards, name="mission_cards"),
    path('symbolcards/', symbol_cards, name="symbol_cards"),
]
