from django.contrib import admin
from decks.models import (
    Deck, AllyCard, ChaosCard,
    CombatClassCard, PowerCard,
    UpgradeCard, SymbolCard,
    MissionCard, LeaderCard,
    SummonedSidekickCard
)
@admin.register(Deck)
class DeckAdmin(admin.ModelAdmin):
    list_display = (
        "deckName",
        "description",
        "deckImg",
    )

@admin.register(AllyCard)
class AllyCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "deck",
        "className",
        "healthPoints",
        # "symbol",
        "comboNum1",
        "comboNum2",
        "comboNum3",
        "cardImg",
        # "backstoryLink",
    )

    def get_deck_name(self, obj):
        return obj.deck.deckName  # replace 'name' with the actual field you want to display
    get_deck_name.short_description = 'Deck Name'  # Sets column name in admin interface

@admin.register(ChaosCard)
class ChaosCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "className",
        "deck",
        "powerRank",
        "healthPoints",
        "defenseRating",
        "symbol1",
        "symbol2",
        "cardImg",
        "backstoryLink",
    )

@admin.register(PowerCard)
class PowerCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "className",
        "deck",
        "powerRank",
        "comboNum1",
        "comboNum2",
        "cardImg",
    )
@admin.register(UpgradeCard)
class UpgradeCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "className",
        "deck",
        "powerRank",
        "comboNum1",
        "comboNum2",
        "cardImg",
    )
@admin.register(SymbolCard)
class SymbolCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "deck",
        "cardImg",
    )

@admin.register(CombatClassCard)
class CombatClassCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "deck",
        "comboNum1",
        "comboNum2",
        "cardImg",
    )

@admin.register(MissionCard)
class MissionCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "deck",
        "symbol1",
        "symbol2",
        "symbol3",
        "symbol4",
        "cardImg",
    )

@admin.register(LeaderCard)
class LeaderCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "deck",
        "cardImg",
    )

@admin.register(SummonedSidekickCard)
class SummonedSidekickCardAdmin(admin.ModelAdmin):
    list_display = (
        "cardName",
        "className",
        "deck",
        "cardImg",
    )
