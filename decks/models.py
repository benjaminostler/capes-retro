from django.db import models


class Deck(models.Model):
    deckName = models.CharField(max_length=150, null=True)
    # ex: brawler cape deck, blaster cape deck, etc.
    description = models.TextField(max_length=150, null=True)
    deckImg = models.ImageField()

    def __str__(self):
        return self.deckName


# Ally Cards: Superhero, Hero, Sidekick
class AllyCard(models.Model):
    cardName = models.CharField(max_length=150)
    className = models.CharField(max_length=150, null=True)
    healthPoints = models.IntegerField(default=1)
    comboNum1 = models.IntegerField(default=0)
    comboNum2 = models.IntegerField(default=0)
    comboNum3 = models.IntegerField(default=0)
    if className == "Superhero":
        healthPoints = 3
    elif className == "Hero":
        healthPoints = 2
    elif className == "Sidekick":
        healthPoints = 1
    symbol = models.CharField(max_length=150, null=True)
    cardImg = models.ImageField(upload_to="upload/")
    backstoryLink = models.URLField(default="https://en.wikipedia.org/wiki/Backstory")
    deck = models.ForeignKey(
        Deck,
        related_name="capecards",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.cardName


# Chaos Deck
class ChaosCard(models.Model):
    cardName = models.CharField(max_length=150)
    className = models.CharField(max_length=150, null=True)
    deck = models.ForeignKey(
        Deck,
        related_name="chaoscards",
        on_delete=models.CASCADE,
    )
    powerRank = models.IntegerField(default=1)
    healthPoints = models.IntegerField(default=1)
    defenseRating = models.IntegerField(default=5)
    if className == "Supervillain":
        defenseRating = 15
        healthPoints = 3
        powerRank = 3
    elif className == "Villain":
        defenseRating = 10
        healthPoints = 2
        powerRank = 2
    # is used in place of threatpoints
    symbol1 = models.CharField(max_length=150, null=True)
    symbol2 = models.CharField(max_length=150, null=True)
    cardImg = models.ImageField(upload_to="upload/")
    backstoryLink = models.URLField(default="https://en.wikipedia.org/wiki/Backstory")

    def __str__(self):
        return self.cardName


class PowerCard(models.Model):  # Deck "Cape" className: "Power"
    cardName = models.CharField(max_length=150)
    className = models.CharField(max_length=150, default="Power")
    deck = models.ForeignKey(
        Deck,
        related_name="powercards",
        on_delete=models.CASCADE,
    )
    powerRank = models.IntegerField(null=True)
    comboNum1 = models.IntegerField(null=True)
    comboNum2 = models.IntegerField(null=True)
    cardImg = models.ImageField(upload_to="upload/")

    def __str__(self):
        return self.cardName


class UpgradeCard(models.Model):  # Deck: "Cape" className: "Upgrade"
    cardName = models.CharField(max_length=150)
    className = models.CharField(max_length=150, default="Upgrade")
    deck = models.ForeignKey(
        Deck,
        related_name="upgradecards",
        on_delete=models.CASCADE,
    )
    powerRank = models.IntegerField(null=True)
    comboNum1 = models.IntegerField(null=True)
    comboNum2 = models.IntegerField(null=True)
    cardImg = models.ImageField(upload_to="upload/")

    def __str__(self):
        return self.cardName


class SymbolCard(models.Model):  # Deck "Symbol"
    # cardName is symbol name
    cardName = models.CharField(max_length=150)
    deck = models.ForeignKey(
        Deck,
        related_name="symbolcards",
        on_delete=models.CASCADE,
    )
    cardImg = models.ImageField(upload_to="upload/")

    def __str__(self):
        return self.cardName


class CombatClassCard(models.Model):  # Deck "Combat Class"
    # cardName: Combat Class Name
    cardName = models.CharField(max_length=150)
    deck = models.ForeignKey(
        Deck,
        related_name="combatclasscards",
        on_delete=models.CASCADE,
    )
    cardImg = models.ImageField(upload_to="upload/")
    comboNum1 = models.IntegerField(null=True)
    comboNum2 = models.IntegerField(null=True)

    def __str__(self):
        return self.cardName


class MissionCard(models.Model):  # Deck "Mission"
    cardName = models.CharField(max_length=150, default="Mission")
    deck = models.ForeignKey(
        Deck,
        related_name="missioncards",
        on_delete=models.CASCADE,
    )
    symbol1 = models.CharField(max_length=150, null=True)
    symbol2 = models.CharField(max_length=150, null=True)
    symbol3 = models.CharField(max_length=150, null=True)
    symbol4 = models.CharField(max_length=150, null=True)
    cardImg = models.ImageField(upload_to="upload/")

    def __str__(self):
        return self.cardName


class LeaderCard(models.Model):  # Deck "Leader"
    cardName = models.CharField(max_length=150)
    cardImg = models.ImageField(upload_to="upload/")
    deck = models.ForeignKey(
        Deck,
        related_name="leadercards",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.cardName


class SummonedSidekickCard(models.Model):  # Deck "Summoned Sidekick"
    # cardName is summoned sidekick name
    cardName = models.CharField(max_length=150)
    className = models.CharField(max_length=150, null=True)
    deck = models.ForeignKey(
        Deck,
        related_name="summonedsidekickcards",
        on_delete=models.CASCADE,
    )
    cardImg = models.ImageField(upload_to="upload/")

    def __str__(self):
        return self.cardName
