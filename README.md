# Capes-Retro



## Getting started

# Create a new virtual environment
python -m venv .venv

# Activate a virtual environment
./.venv/Scripts/Activate.ps1 	# Windows
	#on mac: source ./.venv/bin/activate

# Update pip
python -m pip install --upgrade pip

# Install requirements
pip install -r requirements.txt

## Start the App
python manage.py runserver


# Access the Admin page for your project in browser at address: localhost:8000/admin
# When using the first time, you will need to use powershell to create a super user with username and password. Run the command: “python manage.py createsuperuser” then follow through steps given to create the username and password.
# Use the admin page if you need to remove any of the current decks, cards, etc.

## Flush Command to Reset the Database: The flush command removes all the database data and re-runs any post-synchronization handlers.
python manage.py flush

## To completely reset the Django Database, including removing the data structures
# 1 - Delete all migrations files: Migrations are Django's method of propagating changes made in the model (adding a field, deleting a model, etc.) into our database. Django's migration can be reset by deleting all migration files such as 0001_inital, 0002_delete_song, 0003_delete_album, etc. except the __init__.py files in each project app directory, then dropping the database and creating migration again.
# 2 - Delete db.sqlite3 file: After clearing the migration files, our next step is to delete the db.sqlite3 file. SQLite is an embedded database engine, so deleting a database requires deleting the file from the machine. db.sqlite3 is a database file that will keep all of the data we generated in our project.
# 3 - Make new migrations files: Use the following command to migrate all Django applications.
python manage.py makemigrations
#   ** In some cases, migrations are not executed for the applications. In such cases, we add the application names along with the following command to migrate all the changes in the respective applications.
#   python manage.py makemigrations app_one
#   python manage.py makemigrations app_two
#   python manage.py makemigrations app_three
# 4 - Migrate changes: In the end, we migrate changes creating a new db.sqlite3 database, using the following command.
python manage.py migrate
# 5 - Create your Superuser again by using the following command then following the prompts
python manage.py createsuperuser


## To import cards from csv file, first import the decks
# Run the import_decks.py by running the following command:
 python manage.py runscript import_decks
# Run the import_cards.py by running the following command:
 python manage.py runscript import_cards