from django.urls import path
from players.views import show_player, list_players

urlpatterns = [
	path('detail/', show_player, name="show_player"),
    path('players/', list_players, name="list_players"),
]
