from django.shortcuts import render

def show_player(request):
    return render(request, "detail.html")

def list_players(request):
    return render(request, "players.html")
