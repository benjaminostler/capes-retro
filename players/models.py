from django.db import models

class Player(models.Model):
    playerNum = models.CharField(max_length=150, null=True)
    playerName = models.CharField(max_length=150, null=True)
    threatPts = models.IntegerField(null=True)

    def __str__(self):
        return self.playerName
