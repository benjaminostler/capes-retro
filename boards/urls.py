from django.urls import path
from boards.views import (
    show_board, list_boards,
    # create_board,
    # create_playerboard, create_wargrid,
    # create_hand, create_round, create_phase
    )

urlpatterns = [
	path('detail/', show_board, name="show_board"),
    path('boards/', list_boards, name="list_boards"),
    # path("create/", create_board, name="create_board"),
    # path("create/", create_playerboard, name="create_playerboard"),
    # path("create/", create_wargrid, name="create_wargrid"),
    # path("create/", create_hand, name="create_hand"),
    # path("create/", create_round, name="create_round"),
    # path("create/", create_phase, name="create_phase"),
]
