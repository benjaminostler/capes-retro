from django.db import models

class Board(models.Model):
    boardType = models.CharField(max_length=150, null=True)
    boardName = models.CharField(max_length=150, null=True)
    description = models.TextField(max_length=150, null=True)
    num_lanes = models.IntegerField(null=True)
    num_columns = models.IntegerField(null=True)
    def __str__(self):
        return f'{self.boardType} {self.boardName}'


# class PlayerBoard(Board):

#     def __str__(self):
#         return self.boardName


# class WarGrid(Board):

#     def __str__(self):
#         return self.boardName


# class Hand(Board):
#     playerNum = models.IntegerField()
#     operationPts = models.IntegerField()
#     def __str__(self):
#         return f'Player {self.playerNum}'


# class Round(models.Model):
#     roundname = models.CharField(max_length=150)
#     typeName = models.CharField(max_length=150)
#     description = models.TextField(max_length=400)
#     numPlayers = models.IntegerField()
#     def __str__(self):
#         return self.typeName

# class Phase(Round):
#     phaseName = models.CharField(max_length=150)
#     currentPlayer = models.IntegerField()
#     phase_description = models.TextField(max_length=400)
#     round = models.ForeignKey(
#         Round,
#         related_name="phases",
#         on_delete=models.CASCADE,
#     )
#     def __str__(self):
#         return self.phaseName
