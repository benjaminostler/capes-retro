from django.shortcuts import render, redirect
from boards.models import (
    Board,
    # PlayerBoard, WarGrid, Hand, Round, Phase
)
# from boards.forms import BoardForm, PlayerBoardForm, WarGridForm, HandForm, RoundForm, PhaseForm

def show_board(request):

    return render(request, "detail.html")

def list_boards(request):
    list_boards = Board.objects.all
    context = {
        "list_boards": list_boards,
    }
    return render(request, "boards.html", context)


# def create_board(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = BoardForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             board = form.save()
#             # save
#             board.save()
#         # Redirect to a different page
#         return redirect("boards")
#     else:
#         # Create a new form instance
#         form = BoardForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_board.html", context)


# def create_playerboard(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = PlayerBoardForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             playerboard = form.save()
#             # save
#             playerboard.save()
#         # Redirect to a different page
#         return redirect("boards")
#     else:
#         # Create a new form instance
#         form = PlayerBoardForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_board.html", context)


# def create_wargrid(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = WarGridForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             wargrid = form.save()
#             # save
#             wargrid.save()
#         # Redirect to a different page
#         return redirect("boards")
#     else:
#         # Create a new form instance
#         form = WarGridForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_board.html", context)


# def create_hand(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = HandForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             hand = form.save()
#             # save
#             hand.save()
#         # Redirect to a different page
#         return redirect("hands")
#     else:
#         # Create a new form instance
#         form = HandForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_hand.html", context)


# def create_round(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = RoundForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             round = form.save()
#             # save
#             round.save()
#         # Redirect to a different page
#         return redirect("rounds")
#     else:
#         # Create a new form instance
#         form = RoundForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_round.html", context)

# def create_phase(request):
#     # Check if request is a POST
#     if request.method == "POST":
#         # Create new form instance & fill with values from submission
#         form = PhaseForm(request.POST)
#         # Check if the form is valid
#         if form.is_valid():
#             # save all info from form into variable
#             phase = form.save()
#             # save
#             phase.save()
#         # Redirect to a different page
#         return redirect("rounds")
#     else:
#         # Create a new form instance
#         form = RoundForm()
#     # Add it to the context
#     context = {"form": form}
#     return render(request, "create_phase.html", context)
