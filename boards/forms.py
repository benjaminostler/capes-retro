# from django.forms import ModelForm
# from boards.models import Board, PlayerBoard, WarGrid, Round, Phase, Hand


# class BoardForm(ModelForm):
#     class Meta:
#         model = Board
#         fields = [
#             "name",
#             "type",
#             "description",
#             "lanes",
#             "columns",
#         ]

# class PlayerBoardForm(ModelForm):
#     class Meta:
#         model = PlayerBoard
#         fields = [
#             "name",
#             "type",
#             "description",
#             "lanes",
#             "columns",
#         ]

# class WarGridForm(ModelForm):
#     class Meta:
#         model = WarGrid
#         fields = [
#             "name",
#             "type",
#             "description",
#             "lanes",
#             "columns",
#         ]

# class HandForm(ModelForm):
#     class Meta:
#         model = Hand
#         fields = [
#             "player number",
#             "operation points",
#             "description",
#             "lanes",
#             "columns",
#         ]

# class RoundForm(ModelForm):
#     class Meta:
#         model = Round
#         fields = [
#             "name",
#             "type",
#             "description",
#             "number of players",
#         ]

# class PhaseForm(ModelForm):
#     class Meta:
#         model = Phase
#         fields = [
#             "name",
#             "current player",
#             "description",
#         ]
