# Generated by Django 5.0.3 on 2024-04-05 19:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Board',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('boardType', models.CharField(max_length=150, null=True)),
                ('boardName', models.CharField(max_length=150, null=True)),
                ('description', models.TextField(max_length=150, null=True)),
                ('num_lanes', models.IntegerField(null=True)),
                ('num_columns', models.IntegerField(null=True)),
            ],
        ),
    ]
